![Logo](images/logo.png)

# SwiftCore suppress api matching documentation #

## Matching Rules ##
* The order of datasets being matched against is controlled by the list of DataSetIds in the JobOptions.
* A full name can be set in the surname field (see example code).
* In batch; if a person matches multiple datasets then just the first dataset specified from the list will be returned.
* In single; if a person matches multiple datasets then the number of matches returned is dictated by maxResponseDatasets.
* Full forenames will need to be provided in order to match to Home Mover location files.
* Where ever possible, Title, Forename and Surname data should be supplied. As a minimum, Forename and Surname data should be present. We do not recommend the Surname data only being supplied, even for Surname level matching - match rates will be considerably lower in the event that data is supplied in this way.

## Matching Levels ##
|Name Patterns|Example|Loose|Pragmatic|Moderate|Strict|Ultra Strict|
|---|---|:---:|:---:|:---:|:---:|:---:|
|Forename Forename vs Initial|Anthony Benedict matches A|Yes|Yes|No|No|No|
|Forename Forename vs Initial Initial|Anthony Benedict matches A B|Yes|Yes|Yes|Yes|No|
|Forename Forename vs Forename|Anthony Benedict matches Anthony|Yes|Yes|Yes|No|No|
|Forename Forename vs Forename Initial|Anthony Benedict matches Anthony B|Yes|Yes|Yes|Yes|No|
|Forename Forename vs Initial Forename|Anthony Benedict matches A Benedict|Yes|Yes|Yes|Yes|No|
|Forename Initial vs Initial|Anthony B matches A|Yes|Yes|No|No|No|
|Forename Initial vs Initial Initial|Anthony B matches A B|Yes|Yes|Yes|Yes|No|
|Forename Initial vs Forename|Anthony B matches Anthony|Yes|Yes|Yes|Yes|No|
|Forename Initial vs Initial Forename|Anthony B matches A Benedict|Yes|Yes|Yes|Yes|No|
|Initial Forename vs Initial|A Benedict matches A|Yes|Yes|Yes|No|No|
|Initial Forename vs Initial Initial|A Benedict matches A B|Yes|Yes|Yes|Yes|No|
|Initial Forename vs Forename|A Benedict matches Anthony|Yes|No|No|No|No|
|Forename vs Initial|Anthony matches A|Yes|Yes|No|No|No|
|Forename vs Initial Initial|Anthony matches A B|Yes|Yes|No|No|No|
|Initial Initial vs Initial|A B matches A|Yes|Yes|Yes|No|No|
|Initial vs Initial|A matches A|Yes|Yes|Yes|Yes|No|
|<something> vs <nothing>|Anthony Benedict matches <nothing>|Yes|No|Yes|No|No|
|<nothing> vs <nothing>|<nothing> matches <nothing>|Yes|No|Yes|No|No|
|Allow Diminutive Forename Matching|Sebastian matches Seb|Yes|Yes|Yes|Yes|No|
|Allow Fuzzy Matching (Typos and Phonetics)|Sebastian matches Sbastian|Yes|Yes|Yes|Yes|No|
|Block Marital Status Checking|Miss Mrs|Yes|Yes|No|No|No|
