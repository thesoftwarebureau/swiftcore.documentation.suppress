![Logo](images/logo.png)

# SwiftCore suppress api test mode documentation #

## Test Mode ##
* Test mode allows a User to test applications by calling the api without incurring a charge for download royalties.
* Any work downloaded during testing will be reported on to the Partner/Data Owner, but with an indicator to denote that these jobs were done during a 'Testing' phase.
* There are 3 modes relating to test mode:
    * Normal usage (not in test mode)
    * Test mode: Allowed to download
    * Test mode: Not allowed to download
* Notes:
    * If a User is in test mode and not allowed to download; then any call to the chunk download endpoint will be blocked with a http response of 400 (Bad Request).
    * When creating a new job, the job will be tagged with the same Test mode setting as the User.  So it will not be possible to download a job that was created during 'Test mode'  (if the User was not allowed to download during testing) after changing the User to 'Normal usage' mode.
