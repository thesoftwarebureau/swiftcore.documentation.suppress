![Logo](images/logo.png)

# SwiftCore suppress api FAQs #

## Common FAQs ##
* What is the version parameter for swagger ?
    * The current batch & single api versions are 2.0
* Is this UK specific
    * yes
* What is the time to live for an access token ?
    * 1 hour

## Batch FAQs ##
* What is the difference between the chunk report & chunk download
    * The report returns the number of matches by dataset
    * The download returns:
        * Each match with the dataset it matched against.
        * Any dataset specific additional information eg. new address
* When calling the chunk report endpoint, how do I know whether the chunk has completed matching
    * Response from Get Chunk Report endpoint:
        * Successful completion:
            * Status code: 200
            * Body contains DatasetMatches
            * Header Location element is present and contains uri for the chunk download
        * Still processing:
            * Status code: 200
            * Body is empty
            * Header waitduration element is present denoting how long to wait (in seconds) before retrying the call
                * a waitduration = -1 indicates the chunk processing failed.  It will never be returned.
        * Error:
            * Response status code not ok ie. not 200
* When do we get charged ?
    * you get billed only when you download any matches
* What will we be charged ?
    * you get charged per match that you download
* Why are the download suppression & new address matches returned within an array ?
    * currently we will only return a single suppression or new address match depending upon job dataset hierarchy
    * in the future we may give the option of returning multiple matches
* What is the job iteration ?
    * It represents the number of times the job options have been set or updated
* What is the difference between Flag & Suppress in the job options ?
    * Suppress: where a record is removed from a mailing (data) file, so the individual is not contacted. Note: If a Relocation and/or New Occupier Dataset Type is used when creating a job then matches to these types of Datasets will return the Suppression information to the user along with the new Relocation and/or New Occupier data
    * Flag: where the Suppression information is returned to the user, so that they can update their database/CRM with information to prevent the record being mailed again. Note: If a Relocation and/or New Occupier Dataset Type is used when creating a job then matches to these types of Datasets will return the Suppression information to the user along with the new Relocation and/or New Occupier data. This is a permanent Flag on the database
* Why is the ClientName in the job options mandatory ?
    * The ClientName is to be populated with the End Client name (wherever possible). This is a mandatory field and is needed to comply with Data Provider terms of use, in that they need to have full transparency about which businesses are ultimately accessing their data.

## Single FAQs ##
* Do I have to get a new access token before every single endpoint call ?
    * no,  an access token has a 1 hour lifetime, so you can re-use the token for subsequent calls (within the hour)
* What attribute keys are returned for a dataset ?
    * The attributes returned depend upon the type of dataset eg. a movers dataset will return a 'NewAddress' attribute.  For more information see: [single api documentation](single.md)
