![Logo](images/logo.png)

# SwiftCore batch suppress upgrading from vs2 to vs3 #

## Api changes ##
* Error responses: Replace SwiftCoreApiError with [ProblemDetails](https://datatracker.ietf.org/doc/html/rfc7807) 
    * V2 error response body:
            ```json
            {
                "message": "JobType must be Flag or Suppress"
            }    
            ```
    * V3 error response body:
            ```json
            {
                "title": "SwiftCore Validation Exception",
                "status": 400,
                "detail": "JobType must be Flag or Suppress"
            }    
            ```
* Post Job endpoint responds with new strongly typed response object
    * V2 error response body:
            ```json
            "bc33dbc3-b68b-4aa3-b775-c81d12a41e2a"
            ```
    * V3 response body: 
            ```json
            {
                "jobId": "bc33dbc3-b68b-4aa3-b775-c81d12a41e2a"
            }
            ```
* Post Chunk endpoint responds with new strongly typed response object
    * V2 error response body:
            ```json
            "3fa85f64-5717-4562-b3fc-2c963f66afa6"
            ```
    * V3 response body: 
            ```json
            {
                "chunkId": "3fa85f64-5717-4562-b3fc-2c963f66afa6"
            }
            ```