![Logo](images/logo.png)

# SwiftCore suppress api authentication #

## Overview ##
Authentication uses OAuth2. After authenticating with our OAuth2 server you will recieve an access token which will need to be passed with any api calls.

## Get an api key ##
* Register here: https://portal.swiftcore.net/
* Retrieve your client id & client secret.  Note; you will need to regenerate the secret in order to copy it.
* Contact The Software Bureau to activate your api key
 
## Authentication ##
* Get api client credentials:
    * Login to the SwiftCore portal: https://portal.swiftcore.net/
    * Retrieve you client id & client secret.
* From code, retrieve an authentication token.
    * Call our OAuth2 server passing your client id, client secret and scope of: 'suppress.execute' for batch and 'suppress.single.execute' for single.
    * Pass the authentication token with each endpoint call.
    * Note: the token has a TTL = 1 hour

## Testing Authentication ##
* [Testing with Postman](postman.md)
