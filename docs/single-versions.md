![logo](images/logo.png)

# SwiftCore Single Api Version History #

## 3.0.6 ##
#### 12th June 2024 ####
* V2 retired

## 3.0.1 ##
#### 7th December 2023 ####
* V3 release
* Error responses: Replace SwiftCoreApiError with [ProblemDetails](https://datatracker.ietf.org/doc/html/rfc7807) 

## 2.0.23 ##
#### 31st March 2022 ####
* About/Version endpoint returns version as a json object
* New endpoint: About/DataVersion returns the version of suppression data being currently used

## 2.0.15 ##
#### 27th May 2021 ####
* Attribute formats added for date attributes
* Dataset list now includes attributes and their respective formats

## 2.0.3 ##
#### 8th Jan 2020 ####
* Rate limiting implemented
    * response code 429 returned when exceeded

## 2.0.2 ##
#### 27th Nov 2019 ####
* V2 release
* New address & new occupier returned as attributes
* Swagger updated to OpenAPI 3

## 1.0.4 ##
#### 7th Aug 2019 ####
* Initial release