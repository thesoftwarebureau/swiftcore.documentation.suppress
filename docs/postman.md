![logo](images/logo.png)

# Testing SwiftCore with Postman #

## Get Postman ##
* https://www.getpostman.com/

## Calling SwiftCore ##

### 1. Enter SwiftCore uri https://suppress.swiftcore.net/api/v2.0/dataset/list & set authorization to OAuth 2.0 ###
![postman step 1](images/postman-1.png)

### 2. Enter authentication details from https://id.swiftcore.net/connect/token ###
![postman step 2](images/postman-2.png)

### 3. Use token ###
![postman step 3](images/postman-3.png)

### 4. Check header contains authorization token, send the api request & check the results ###
![postman step 4](images/postman-4.png)

