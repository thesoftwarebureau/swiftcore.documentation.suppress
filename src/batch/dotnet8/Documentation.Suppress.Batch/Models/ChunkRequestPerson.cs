﻿using System.ComponentModel.DataAnnotations;

namespace SwiftCore.Documentation.Suppress.Batch.Models;

/// <summary>
/// Job chunk person.
/// </summary>
public class ChunkRequestPerson
{
    // Must have parameter-less constructor
    public ChunkRequestPerson() { }

    /// <summary>
    /// Unique identifier for this person.
    /// </summary>
    [Required]
    [MaxLength(50)]
    public string CustomerUrn { get; set; }

    public string Title { get; set; }
    public string Forename1 { get; set; }
    public string Forename2 { get; set; }
    public string Surname { get; set; }
    public string Address1 { get; set; }
    public string Address2 { get; set; }
    public string Address3 { get; set; }
    public string Address4 { get; set; }
    public string Address5 { get; set; }
    public string Address6 { get; set; }
    public string Address7 { get; set; }
    public string Address8 { get; set; }
    public string Address9 { get; set; }
}