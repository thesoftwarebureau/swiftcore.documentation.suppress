﻿using System.Text.Json.Serialization;

namespace SwiftCore.Documentation.Suppress.Batch.Models;

/// <summary>
/// The object representing the details about a ValidationException caught by a service implementing RFC 7807.
/// </summary>
public class ProblemDetails
{
    [JsonPropertyName("error")]
    public string Error { get; set; }

    [JsonPropertyName("detail")]
    public string Detail { get; set; }

    [JsonPropertyName("status")]
    public int Status { get; set; }
}