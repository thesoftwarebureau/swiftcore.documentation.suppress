﻿namespace SwiftCore.Documentation.Suppress.Batch.Models;

/// <summary>
/// Job chunk response returned by the client api to the customer.
/// </summary>
public abstract class ChunkResponse : IChunkResponse
{
    // Must have parameter-less constructor
    protected ChunkResponse() { }

    public Guid ChunkId { get; set; }
    public Guid JobId { get; set; }


    public string JobRef { get; set; }
    public string JobType { get; set; }

    public int ChunkInputQuantity { get; set; }
    public int ChunkMatchQuantity { get; set; }
    public int Iteration { get; set; }
    public DateTime DateSubmitted { get; set; }
    public DateTime DateFinishedMatching { get; set; }
}