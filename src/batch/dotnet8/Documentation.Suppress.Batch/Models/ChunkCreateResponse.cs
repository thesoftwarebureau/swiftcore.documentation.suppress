﻿namespace SwiftCore.Documentation.Suppress.Batch.Models;

/// <summary>
/// ChunkId created
/// </summary>
public class ChunkCreateResponse
{
    public Guid ChunkId { get; set; }
}