﻿namespace SwiftCore.Documentation.Suppress.Batch.Models;

/// <summary>
/// Dataset options.
/// </summary>
public class DatasetOptionsResponse
{
    // Must have parameter-less constructor
    public DatasetOptionsResponse() { }

    public bool CanSetDataSetHierarchy { get; set; }
}