﻿namespace SwiftCore.Documentation.Suppress.Batch.Models;

/// <summary>
/// JobId created
/// </summary>
public class JobCreateResponse
{
    public Guid JobId { get; set; }
}