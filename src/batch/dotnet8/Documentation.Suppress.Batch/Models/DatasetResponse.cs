﻿namespace SwiftCore.Documentation.Suppress.Batch.Models;

/// <summary>
/// Dataset info.
/// </summary>
public class DatasetResponse
{
    // Must have parameter-less constructor
    public DatasetResponse() { }

    public string DataSetId { get; set; }
    public string DatasetName { get; set; }
    public string DatasetDescription { get; set; }
    public string DatasetServiceDescription { get; set; }
    public string DataSetDisplayName { get; set; }
    public string SupplierName { get; set; }
    public string DataSetRef { get; set; }
    public List<DatasetAttributeResponse> DataSetAttributes { get; set; }
}