﻿namespace SwiftCore.Documentation.Suppress.Batch.Models;

public class ChunkDownloadResponseDatasetAttribute
{
    public string Key { get; set; }
    public List<string> Format { get; set; } = new List<string>();
    public List<string> Value { get; set; }
}