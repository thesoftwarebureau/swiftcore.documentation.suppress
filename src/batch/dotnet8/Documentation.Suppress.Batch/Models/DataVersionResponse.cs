﻿namespace SwiftCore.Documentation.Suppress.Batch.Models;

public class DataVersionResponse
{
    public string DataVersion { get; set; }
}