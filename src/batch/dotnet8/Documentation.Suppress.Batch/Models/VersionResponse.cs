﻿namespace SwiftCore.Documentation.Suppress.Batch.Models;

public class VersionResponse
{
    public string Version { get; set; }
}