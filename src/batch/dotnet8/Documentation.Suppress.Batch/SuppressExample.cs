﻿using System.Text;
using SwiftCore.Documentation.Suppress.Batch.Models;
using System.Text.Json;
using System.Net.Http.Headers;

namespace SwiftCore.Documentation.Suppress.Batch;

internal class SuppressExample
{
    private const string ClientId = @"YOUR-CLIENT-ID-HERE";        // TODO: Set your client id & secret 
    private const string ClientSecret = @"YOUR-CLIENT-SECRET-HERE";

    private const string ApiUrl = "https://suppress.swiftcore.net/api/v3.0";
    private const string OAuth2ServerUrl = "https://id.swiftcore.net";

    private const string Scope = "suppress.execute";


    public async Task GetVersionAsync()
    {
        using var httpClient = new HttpClient();

        const string url = $"{ApiUrl}/about/version";
        var response = await httpClient.GetAsync(url);
        if (!response.IsSuccessStatusCode) throw new ApplicationException("Api call failed");

        var versionResponse = await response.Content.ReadAsAsync<VersionResponse>();
        Console.WriteLine($"Version: {versionResponse.Version}");
    }


    public async Task GetDataVersionAsync()
    {
        using var httpClient = new HttpClient();

        const string url = $"{ApiUrl}/about/dataversion";
        var response = await httpClient.GetAsync(url);
        if (!response.IsSuccessStatusCode) throw new ApplicationException("Single api call failed");

        var dataVersionResponse = await response.Content.ReadAsAsync<DataVersionResponse>();
        Console.WriteLine($"DataVersion: {dataVersionResponse.DataVersion}");
    }

    
    public async Task<string> GetAccessTokenAsync()
    {
        using var httpClient = HttpClientFactory.Create();

        // Create auth server request token
        var formContent = new List<KeyValuePair<string, string>>
        {
            new KeyValuePair<string, string>("client_id", ClientId),
            new KeyValuePair<string, string>("client_secret", ClientSecret),
            new KeyValuePair<string, string>("scope", Scope),
            new KeyValuePair<string, string>("grant_type", "client_credentials")
        };

        // Request the access token token
        const string tokenEndpoint = $"{OAuth2ServerUrl}/connect/token";
        var request = new HttpRequestMessage(HttpMethod.Post, tokenEndpoint) { Content = new FormUrlEncodedContent(formContent) };
        var response = await httpClient.SendAsync(request);

        // Failed:
        if (!response.IsSuccessStatusCode)
        {
            var json = await response.Content.ReadAsStringAsync();
            var problemDetails = JsonSerializer.Deserialize<ProblemDetails>(json);
            throw new ApplicationException("Api call failed: " + problemDetails.Detail);
        }

        // Success:
        await using var responseStream = await response.Content.ReadAsStreamAsync();
        var tokenResponse = await JsonSerializer.DeserializeAsync<TokenResponse>(responseStream);
        if (tokenResponse is null)
        {
            const string errorMessage = $"Failed converting the response to a type of: {nameof(TokenResponse)}";
            throw new HttpRequestException(errorMessage);
        }

        return tokenResponse.access_token;
    }


    public async Task<List<DatasetResponse>> GetDatasetListAsync(string token)
    {
        using var httpClient = new HttpClient();

        httpClient.SetBearerToken(token);
        const string url = $"{ApiUrl}/dataset/list";
        var response = await httpClient.GetAsync(url);

        if (!response.IsSuccessStatusCode)
        {
            var json = await response.Content.ReadAsStringAsync();
            var problemDetails = JsonSerializer.Deserialize<ProblemDetails>(json);
            throw new ApplicationException("Api call failed: " + problemDetails.Detail);
        }

        var datasets = await response.Content.ReadAsAsync<List<DatasetResponse>>();
        Console.WriteLine($"Available Datasets: {datasets.Count}");
        return datasets;
    }


    public async Task<Guid> CreateJobAsync(string token)
    {
        try
        {
            using var httpClient = new HttpClient();

            httpClient.SetBearerToken(token);

            // Create job
            var jobOptions = new JobCreateRequest
            {
                JobRef = "My Cust Ref",
                ClientName = "My Client name",
                JobType = "Suppress",
                DataSets = new List<DatasetRequest>
                {
                    new DatasetRequest {DataSetId = "REAB", MatchLevel = "IndividualLoose"},
                    new DatasetRequest {DataSetId = "REAM", MatchLevel = "IndividualLoose"},
                    new DatasetRequest {DataSetId = "REAA", MatchLevel = "IndividualLoose"},
                    new DatasetRequest {DataSetId = "REAO", MatchLevel = "IndividualLoose"},
                    new DatasetRequest {DataSetId = "REAT", MatchLevel = "IndividualLoose"}
                }
            };
            var stringPayload = await Task.Run(() => JsonSerializer.Serialize(jobOptions));
            var httpContent = new StringContent(stringPayload, Encoding.UTF8, "application/json");

            var httpResponse = await httpClient.PostAsync($"{ApiUrl}/job", httpContent);
            if (!httpResponse.IsSuccessStatusCode)
            {
                var json = await httpResponse.Content.ReadAsStringAsync();
                var problemDetails = JsonSerializer.Deserialize<ProblemDetails>(json);
                throw new ApplicationException("Api call failed: " + problemDetails.Detail);
            }

            var content = await httpResponse.Content.ReadAsAsync<JobCreateResponse>();
            var jobId = content.JobId;

            Console.WriteLine($"Created Job;   Id: {jobId}");

            return jobId;
        }
        catch (Exception e)
        {
            throw new ApplicationException(e.Message);
        }
    }


    public async Task<Uri> PostChunkAsync(Guid jobId, string token)
    {
        using var httpClient = new HttpClient();

        httpClient.SetBearerToken(token);
        var personList = new List<ChunkRequestPerson>
        {
            new ChunkRequestPerson
            {
                CustomerUrn = "URN0001", Title = "Mr", Forename1 = "Fred", Surname = "Smith",
                Address1 = "1 The High Street", Address2 = "AnyTown", Address3 = "AB1 2CD"
            },
            new ChunkRequestPerson
            {
                CustomerUrn = "URN0002", Surname = "mrs alice smith", Address1 = "2 The High Street",
                Address3 = "AB1 2CD"
            }
        };

        var chunkRequest = new ChunkRequest
        {
            JobId = jobId,
            People = personList
        };

        var payload = await Task.Run(() => JsonSerializer.Serialize(chunkRequest));
        var httpContent = new StringContent(payload, Encoding.UTF8, "application/json");
        var httpResponse = await httpClient.PostAsync($"{ApiUrl}/chunk", httpContent);

        if (!httpResponse.IsSuccessStatusCode)
        {
            var json = await httpResponse.Content.ReadAsStringAsync();
            var problemDetails = JsonSerializer.Deserialize<ProblemDetails>(json);
            throw new ApplicationException(problemDetails.Detail);
        }

        // could return chunk id
        var content = await httpResponse.Content.ReadAsAsync<ChunkCreateResponse>();
        var chunkId = content.ChunkId;
        Console.WriteLine($"Submitted Chunk; Id: {chunkId}");

        var reportUri = httpResponse.Headers.Location;
        return reportUri;
    }


    public async Task<Uri> PollForChunkReportAsync(Uri reportUri, string token)
    {
        using var httpClient = new HttpClient();

        httpClient.SetBearerToken(token);

        var chunkPoller = new ChunkPoller(httpClient);
        var chunkPollerResults = await chunkPoller.Poll<ChunkReportResponse>(reportUri);
        var chunkReportResponse = (ChunkReportResponse) chunkPollerResults.ChunkResponse;

        Console.WriteLine(
            $"Retrieved Chunk Report; Id: {chunkReportResponse.ChunkId} matched: {chunkReportResponse.ChunkMatchQuantity}");

        Console.WriteLine("    Chunk Results:");
        Console.WriteLine($"    ChunkId:        {chunkReportResponse.ChunkId}");
        Console.WriteLine($"    JobId:          {chunkReportResponse.JobId}");
        Console.WriteLine($"    Job Ref:        {chunkReportResponse.JobRef}");
        Console.WriteLine($"    Job Type:       {chunkReportResponse.JobType}");
        Console.WriteLine($"    Chunk Input:    {chunkReportResponse.ChunkInputQuantity}");
        Console.WriteLine($"    Chunk Matches:  {chunkReportResponse.ChunkMatchQuantity}");
        Console.WriteLine($"    Iteration:      {chunkReportResponse.Iteration}");
        Console.WriteLine();
        if (chunkReportResponse.DatasetMatches.Count > 0)
        {
            Console.WriteLine("    Matches:");
            foreach (var datasetMatch in chunkReportResponse.DatasetMatches)
            {
                Console.WriteLine($"        {datasetMatch.DatasetId}  {datasetMatch.Quantity}");
            }
        }

        var downloadUri = chunkPollerResults.Location;
        return downloadUri;
    }


    public async Task<ChunkDownloadResponse> PollForChunkResultsAsync(Uri downloadUri, string token)
    {
        using var httpClient = new HttpClient();

        httpClient.SetBearerToken(token);

        var chunkPoller = new ChunkPoller(httpClient);
        var chunkPollerResults = await chunkPoller.Poll<ChunkDownloadResponse>(downloadUri);
        var chunkResponse = (ChunkDownloadResponse) chunkPollerResults.ChunkResponse;

        Console.WriteLine(
            $"Retrieved Chunk Download; Id: {chunkResponse.ChunkId} matched: {chunkResponse.PersonMatches.Count}");

        Console.WriteLine(
            "--------------------------------------------------------------------------------");
        Console.WriteLine("Results:");
        foreach (var chunkResponseResult in chunkResponse.PersonMatches)
        {
            Console.WriteLine("    ----------------------------------------");
            Console.WriteLine($"    CustomerUrn:   {chunkResponseResult.CustomerUrn}");

            // matches
            if (chunkResponseResult.Datasets?.Count > 0)
            {
                Console.WriteLine("    Datasets:");
                foreach (var datasetMatch in chunkResponseResult.Datasets)
                {
                    var matchList = new StringBuilder();
                    Console.WriteLine($"        DatasetId: {datasetMatch.DatasetId},");
                    Console.WriteLine($"        MatchLevel: {datasetMatch.MatchLevel},");
                    Console.WriteLine("        Attributes:");
                    foreach (var attribute in datasetMatch.Attributes)
                    {
                        var attributeValues = new StringBuilder();
                        foreach (var value in attribute.Value)
                        {
                            attributeValues.Append($"{value},");
                        }

                        var attributeFormats = new StringBuilder();
                        foreach (var format in attribute.Format)
                        {
                            attributeFormats.Append($"{format},");
                        }

                        Console.WriteLine($"            {attribute.Key}:{attributeValues} [{attributeFormats}]");
                    }
                }
            }

            Console.WriteLine("    ----------------------------------------");
        }

        Console.WriteLine(
            "--------------------------------------------------------------------------------");

        return chunkResponse;
    }
}


internal static class ExtensionHelpers
{
    internal static void SetBearerToken(this HttpClient httpClient, string token)
    {
        httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
    }
}