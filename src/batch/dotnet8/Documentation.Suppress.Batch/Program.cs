﻿namespace SwiftCore.Documentation.Suppress.Batch;

/// <summary>
/// Example of requesting an OAuth2 access token and calling suppress batch api endpoints.
/// </summary>
/// <remarks>
/// Note that this is not production code.
/// Recommendations for production code:
/// 1: use a retry mechanism eg polly
/// 2: use httpClientFactory for re-using http client instances
/// 3: watch out for the token TTL which is 1 hour
/// </remarks>
class Program
{
    static async Task Main(string[] args)
    {
        try
        {
            var example = new SuppressExample();
            await example.GetVersionAsync();
            await example.GetDataVersionAsync();
            var token = await example.GetAccessTokenAsync();
            await example.GetDatasetListAsync(token);
            var jobId = await example.CreateJobAsync(token);
            var reportUri = await example.PostChunkAsync(jobId, token);
            var downloadUri = await example.PollForChunkReportAsync(reportUri, token);
            var response = await example.PollForChunkResultsAsync(downloadUri, token);

        }
        catch (AggregateException ae)
        {
            foreach (var innerException in ae.InnerExceptions)
            {
                Console.WriteLine(innerException.InnerException?.Message ?? innerException.Message);
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }
        finally
        {
            Console.WriteLine("all done; press 'enter' to finish...");
            Console.ReadLine();
        }
    }
}