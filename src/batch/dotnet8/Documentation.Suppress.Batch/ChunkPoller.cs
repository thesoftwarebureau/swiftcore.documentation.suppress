﻿using System.Net.Http.Headers;
using System.Text.Json;
using SwiftCore.Documentation.Suppress.Batch.Models;

namespace SwiftCore.Documentation.Suppress.Batch;

internal class ChunkPoller
{
    private readonly HttpClient _client;
      
    public ChunkPoller(HttpClient client)
    {
        _client = client;
    }


    /// <summary>
    /// Calls either the report or download chunk endpoint.
    /// </summary>
    public async Task<ChunkPollerResults> Poll<T>(Uri chunkEndpointUri) where T : IChunkResponse
    {
        HttpResponseMessage httpResponse = null;
        T chunkResponse;

        while (true)
        {
            httpResponse = await _client.GetAsync(chunkEndpointUri);
            if (!httpResponse.IsSuccessStatusCode)
            {
                var json = await httpResponse.Content.ReadAsStringAsync();
                var problemDetails = JsonSerializer.Deserialize<ProblemDetails>(json);
                throw new ApplicationException(problemDetails.Detail);
            }

            chunkResponse = await httpResponse.Content.ReadAsAsync<T>();
            if (chunkResponse != null) break;

            //chunk Not yet ready: retry
            var waitDurationSeconds = GetWaitDuration(httpResponse);
            Thread.Sleep(waitDurationSeconds);
        }

        // Return chunk result
        var chunkPollerResults = new ChunkPollerResults
        {
            ChunkResponse = chunkResponse,
            Location = httpResponse.Headers.Location
        };
        return chunkPollerResults;
    }


    private TimeSpan GetWaitDuration(HttpResponseMessage httpResponse)
    {
        var headerWaitDuration = GetWaitDurationFromResponseHeader(httpResponse.Headers);
        return TimeSpan.FromSeconds(headerWaitDuration);
    }


    private double GetWaitDurationFromResponseHeader(HttpResponseHeaders responseHeaders)
    {
        double waitDuration = 2;
        if (responseHeaders.TryGetValues("WaitDuration", out var headerWaitDuration))
        {
            var headerValue = headerWaitDuration.FirstOrDefault();
            if (headerValue != null)
            {
                if (double.TryParse(headerValue, out var result))
                {
                    waitDuration = result;
                }
            }
        }
        return waitDuration;
    }

}