﻿namespace Documentation.Suppress.Single.Models;

public class DataVersionResponse
{
    public string DataVersion { get; set; }
}