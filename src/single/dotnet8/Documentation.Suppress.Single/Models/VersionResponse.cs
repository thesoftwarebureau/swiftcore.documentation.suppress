﻿namespace Documentation.Suppress.Single.Models;

public class VersionResponse
{
    public string Version { get; set; }
}