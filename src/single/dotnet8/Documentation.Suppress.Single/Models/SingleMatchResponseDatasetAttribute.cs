﻿namespace Documentation.Suppress.Single.Models;

/// <summary>
/// Represents an attribute for a matched dataset for the single api
/// </summary>
public class SingleMatchResponseDatasetAttribute
{
    public string Key { get; set; }
    public List<string> Format { get; set; } = new List<string>();
    public List<string> Value { get; set; }
}