﻿namespace Documentation.Suppress.Single.Models;

/// <summary>
/// Dataset attribute info returned by the client api to the customer.
/// </summary>
public class DatasetAttributeResponse
{
    public string Key { get; set; } = string.Empty;

    /// <summary>
    /// The format is optional.
    /// It is only returned when populated.
    /// It is used for date fields to indicate the date format
    /// </summary>
    public string Format { get; set; } = string.Empty;
}