﻿using System.Text;
using Documentation.Suppress.Single.Models;
using System.Text.Json;
using System.Net.Http.Headers;

namespace Documentation.Suppress.Single;

public class SingleCallExample
{
    private const string ClientId = @"YOUR-CLIENT-ID-HERE";        // TODO: Set your client id & secret 
    private const string ClientSecret = @"YOUR-CLIENT-SECRET-HERE";

    private const string ApiUrl = "https://suppress-single.swiftcore.net/api/v3.0";
    private const string OAuth2ServerUrl = "https://id.swiftcore.net";
    private const string Scope = "suppress.single.execute";


    public async Task<string> GetAccessTokenAsync()
    {
        using var httpClient = HttpClientFactory.Create();

        // Create auth server request token
        var formContent = new List<KeyValuePair<string, string>>
        {
            new KeyValuePair<string, string>("client_id", ClientId),
            new KeyValuePair<string, string>("client_secret", ClientSecret),
            new KeyValuePair<string, string>("scope", Scope),
            new KeyValuePair<string, string>("grant_type", "client_credentials")
        };

        // Request the access token token
        const string tokenEndpoint = $"{OAuth2ServerUrl}/connect/token";
        var request = new HttpRequestMessage(HttpMethod.Post, tokenEndpoint) { Content = new FormUrlEncodedContent(formContent) };
        var response = await httpClient.SendAsync(request);

        // Failed:
        if (!response.IsSuccessStatusCode)
        {
            await using var responseErrorStream = await response.Content.ReadAsStreamAsync();
            var errorResponse = await JsonSerializer.DeserializeAsync<Dictionary<string, string>>(responseErrorStream);
            string errorMessage;
            if (errorResponse is not null && errorResponse.TryGetValue("message", out var swiftCoreErrorMessage))
            {
                errorMessage = swiftCoreErrorMessage;
            }
            else
            {
                errorMessage = "Authentication failed";
            }
            throw new HttpRequestException(errorMessage);
        }

        // Success:
        await using var responseStream = await response.Content.ReadAsStreamAsync();
        var tokenResponse = await JsonSerializer.DeserializeAsync<TokenResponse>(responseStream);
        if (tokenResponse is null)
        {
            const string errorMessage = $"Failed converting the response to a type of: {nameof(TokenResponse)}";
            throw new HttpRequestException(errorMessage);
        }

        return tokenResponse.access_token;
    }


    public async Task GetVersionAsync()
    {
        using var httpClient = new HttpClient();

        const string url = $"{ApiUrl}/about/version";
        var response = await httpClient.GetAsync(url);
        if (!response.IsSuccessStatusCode) throw new ApplicationException("Single api call failed");

        var versionResponse  = await response.Content.ReadAsAsync<VersionResponse>();
        Console.WriteLine($"Version: {versionResponse.Version}");
    }


    public async Task GetDataVersionAsync()
    {
        using var httpClient = new HttpClient();

        const string url = $"{ApiUrl}/about/dataversion";
        var response = await httpClient.GetAsync(url);
        if (!response.IsSuccessStatusCode) throw new ApplicationException("Single api call failed");

        var dataVersionResponse  = await response.Content.ReadAsAsync<DataVersionResponse>();
        Console.WriteLine($"DataVersion: {dataVersionResponse.DataVersion}");
    }


    public async Task GetDatasetListAsync(string token)
    {
        using var httpClient = new HttpClient();

        httpClient.SetBearerToken(token);

        const string url = $"{ApiUrl}/dataset/list";
        var response = await httpClient.GetAsync(url);
        if (!response.IsSuccessStatusCode)
        {
            var error = await response.Content.ReadAsAsync<ProblemDetails>();
            throw new ApplicationException("Single api call failed: " + error.Detail);
        }

        var datasets = await response.Content.ReadAsAsync<List<DatasetResponse>>();
        Console.WriteLine($"Available Datasets: {datasets.Count}");
    }


    public async Task PostSingleMatchAsync(string token)
    {
        using var httpClient = new HttpClient();

        httpClient.SetBearerToken(token);

        var request = new SingleMatchRequest
        {
            CustomerUrn  = Guid.NewGuid().ToString(),
            ClientName   = "YourClient",
            // See swagger for MatchLevel options
            DataSets = new List<DatasetRequest>
            {
                new DatasetRequest(){DataSetId = "REAB", MatchLevel = "IndividualLoose"},
                new DatasetRequest(){DataSetId = "REAM", MatchLevel = "Surname"},
                new DatasetRequest(){DataSetId = "REAA", MatchLevel = "IndividualModerate"},
                new DatasetRequest(){DataSetId = "REAO", MatchLevel = "IndividualLoose"},
                new DatasetRequest(){DataSetId = "REAT", MatchLevel = "IndividualLoose"}
            },
            Forename1    = "Fred",
            Surname      = "Smith",
            Address1     = "1 HIGH STREET",
            Address2     = "AB1 1DC"
        };
                
        var stringPayload = await Task.Run(() => JsonSerializer.Serialize(request));
        var httpContent   = new StringContent(stringPayload, Encoding.UTF8, "application/json");
        var url           = $"{ApiUrl}/single";
        var response      = await httpClient.PostAsync(url, httpContent);
        if (!response.IsSuccessStatusCode)
        {
            var error = await response.Content.ReadAsAsync<ProblemDetails>();
            throw new ApplicationException("Single api call failed: "+ error.Detail);
        }
                
        var matchResponse = await response.Content.ReadAsAsync<SingleMatchResponse>();
        Console.WriteLine($"Matched Datasets: {matchResponse.Datasets.Count}");
    }
}


internal static class ExtensionHelpers
{
    internal static void SetBearerToken(this HttpClient httpClient, string token)
    {
        httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
    }
}