﻿namespace Documentation.Suppress.Single;

/// <summary>
/// Example of requesting an OAuth2 access token and calling single api endpoints.
/// </summary>
/// <remarks>
/// Note that this is not production code.
/// Recommendations for production code:
/// 1: use a retry mechanism eg polly
/// 2: use httpClientFactory for re-using http client instances
/// 3: watch out for the token TTL which is 1 hour
/// </remarks>
class Program
{
    static async Task Main(string[] args)
    {
        var singleCallExample = new SingleCallExample();
        var token = await singleCallExample.GetAccessTokenAsync();
        await singleCallExample.GetVersionAsync();
        await singleCallExample.GetDataVersionAsync();
        await singleCallExample.GetDatasetListAsync(token);
        await singleCallExample.PostSingleMatchAsync(token);
    }
}