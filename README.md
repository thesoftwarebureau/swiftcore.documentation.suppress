![Logo](docs/images/logo.png)

# SwiftCore suppress api documentation #

## Overview ##
SwiftCore suppress is a RESTful api allowing the matching of names and addresses against UK suppression & new address datasets.
We offer 2 endpoints for differing use cases:

* #### [Single:](docs/single.md) ####
    * Matching a single name and address
    * [visual studio code rest client example](swiftCoreSuppressSingle.http)
    * Swagger: https://suppress-single.swiftcore.net/
    * Documentation: [Single api documentation](docs/single.md)
* ####  [Batch:](docs/batch.md) ####
    * Matching multiple names and addresses
    * [visual studio code rest client example](swiftCoreSuppressBatch.http)
    * Swagger: https://suppress.swiftcore.net/
    * Documentation: [Batch api documentation](docs/batch.md)


## Authentication ##
* [authentication documentation](docs/authentication.md)

## Matching ##
* [matching documentation](docs/matching.md)

## Attributes ##
* [attributes documentation](docs/attributes.md)

## Test Mode ##
* [test mode documentation](docs/test-mode.md)

## Report Api ##
* About: the report api allows querying your job download history
* Authentication: is the same as for suppress api with one exception:
    * The requested scope must be 'suppress.report'
* Endpoints:
    * See swagger documentation: https://suppress-report.swiftcore.net/swagger

## FAQs ##
* [faq's](docs/faqs.md)
